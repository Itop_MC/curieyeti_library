<?php
/*
  Disable SSL for IMAP/SMTP
  If your IMAP/SMTP servers are on the same host or are connected via a secure network, not using SSL connections improves performance. So don't use "ssl://" or "tls://" urls for 'default_host' and 'smtp_server' config options.
 */
$currentPath = \getcwd();
if (\file_exists(\dirname(__FILE__) . '/../../../../../include/ConfigUtils.php')) {
    \chdir(\dirname(__FILE__) . '/../../../../../');
} elseif (\file_exists(\dirname(__FILE__) . '/../../../../../../include/ConfigUtils.php')) {
    \chdir(\dirname(__FILE__) . '/../../../../../../');
} elseif (\file_exists(\dirname(__FILE__) . '/../../../../../../../include/ConfigUtils.php')) {
    \chdir(\dirname(__FILE__) . '/../../../../../../../');
} else {
    exit('ERROR en: ' . __FILE__ . ':' . __LINE__);
}

if (\class_exists('\App\Config')) {
    if (true === \App\Config::debug('MAILER_DEBUG') || false) {
        \ini_set('display_errors', 'On');
        \ini_set('html_errors', 'On');
        \error_reporting(32759);
    } else {
        \ini_set('display_errors', 'Off');
        \ini_set('html_errors', 'Off');
        \error_reporting(0);
    }
} else {
    \ini_set('display_errors', 'Off');
    \ini_set('html_errors', 'Off');
    \error_reporting(0);
}

include_once 'include/ConfigUtils.php';
$config = \App\Config::module('OSSMail');
\chdir($currentPath);
